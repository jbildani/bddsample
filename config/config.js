const path = require("path");
const jsonReports = process.cwd() + "/reports/json";
const Reporter = require("../support/reporter");

exports.config = {
  //seleniumAddress: "http://localhost:4444/wd/hub",
  //seleniumServerJar: 'C:\Users\BILDANI\AppData\Roaming\npm\node_modules\protractor\node_modules\webdriver-manager\selenium\selenium-server-standalone-3.141.59.jar',
  // directConnect: true,
  baseUrl: "https://openweathermap.org",  
  capabilities: {
    browserName: process.env.TEST_BROWSER_NAME || "chrome",
	chromeOptions: {
  args: ["--headless", "--disable-gpu", "--window-size=800x600"]
	}
  
  },
  framework: "custom",
  frameworkPath: require.resolve("protractor-cucumber-framework"),
  specs: ["../features/*.feature"],
  exclude: "../features/database.feature",
  // resultJsonOutputFile: "./reports/json/protractor_report.json",
  onPrepare: function() {
    browser.ignoreSynchronization = true;
    browser.manage().window().maximize();
    global.weatherMap= require("../pages/WeatherMap.js");
    global.elementHelper=require("../support/ElementHelper.js");
    global.chai=require('chai');
    global.chaiAsPromised=require('chai-as-promised');
    chai.use(chaiAsPromised);    
    global.expect = chai.expect;
    require('babel-register');
    Reporter.createDirectory(jsonReports);
  },
  cucumberOpts: {
    strict: true,
    format: 'json:./reports/json/cucumber_report.json',
    require: ["../stepDefinitions/*.js", "../support/*.js"],
    tags: "(@OpenWeatherScenario)" 
  },
  onComplete: function () {
    Reporter.createHTMLReport();
  }
};
