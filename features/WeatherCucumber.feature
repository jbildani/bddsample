Feature: To Validate the Weather in the City and verify important information

  @OpenWeatherScenario
  Scenario: Important Information should be available in OpenWeatherMap 
      Given I am on weather map application page
      Then The options like SignIn,SignUp,Weather, Map ,Guide, API and City input etc all these should be present

  @OpenWeatherScenario
  Scenario: Searching Invalid City to find weather
      Given Searching "Invalid" City
      Then Search result should "Not found" with any weather details


  @OpenWeatherScenario
  Scenario: Searching valid City to find weather details
      Given Searching "valid" City
      Then Search result should "found" with any weather details


