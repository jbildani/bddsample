

class WeatherMap{
    constructor(){
        
        this.strSignInLbl="Sign In";
        this.strSignUpLbl="Sign Up";
        this.strWeatherInYourCity="Weather in your city"
        this.strLinkLbls="//a[contains(text(),'yyy')]";
        this.searchCity=element(by.xpath("//label[contains(text(),'Search')]/following-sibling::input"));
        this.searchBtn=element(by.xpath("//button[contains(text(),'Search')]"));
        this.strTemperatureLbl=element(by.xpath("//p[contains(text(),'temperature')]"));
        this.strTemperatureInCelsius=element(by.xpath("//p[contains(text(),'temperature')]//span[contains(@class,'badge')]"));

        this.strHeaderLbl="navbar-brand";
        this.strMenuLabelsCss=".nav__link.bg-hover-color";


        /**This method is used to validate the Menu , sign In and Sign up objects   */

        this.validateObjects=function(arrMenus,strMenuCssObj,strLinkLbl)
        {
            try{
            
            element.all(by.cssContainingText(strMenuCssObj,"")).then(function(elements)
            {
                elements.forEach(element => {
                    element.getText().then(function(text)
                    {                    
                        expect(arrMenus.includes(text)).to.be.true;
                    })

            
                });

            })
            expect(element(by.xpath(strLinkLbl.replace("yyy",weatherMap.strSignInLbl))).isPresent()).to.eventually.become(true);
            expect(element(by.xpath(strLinkLbl.replace("yyy",weatherMap.strSignUpLbl))).isPresent()).to.eventually.become(true);
            expect(element(by.xpath(strLinkLbl.replace("yyy",weatherMap.strWeatherInYourCity))).isPresent()).to.eventually.become(true);
        }
    catch(e)
    {
        console.log(e)
    }
    }
     
    this.enterValue=function(strSearchObj,strValue)    
    {
        try{
                strSearchObj.clear();
                strSearchObj.sendKeys(strValue);
           }catch(e)
           {
               console.log(e);
            }
    }

    this.clickOnObject=function(strObject)
    {
        try{
            strObject.click()
            
        }catch(e)
        {
            console.log(e)
        }

    }


    }
}

module.exports= new WeatherMap();
