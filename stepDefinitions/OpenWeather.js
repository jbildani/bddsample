import Globals from '../support/Globals';
import { browser } from 'protractor';
import {Given, When, Then } from "cucumber";

// Chai
const globals = new Globals();
const expect = globals.expect;

var {setDefaultTimeout} = require('cucumber');

setDefaultTimeout(60 * 10000);

Given(/^I am on weather map application page$/, function(callback){
    expect(element(by.className(weatherMap.strHeaderLbl)).isPresent()).to.eventually.become(true);
    browser.sleep(100).then(callback);
});

Then(/^The options like SignIn,SignUp,Weather, Map ,Guide, API and City input etc all these should be present$/, function(callback) {
    
   var arrMenus=["Weather","Maps","Guide","API","Price","Partners","Stations","Widgets","Blog"];
    
    //Here validating objects (Sign In links,label, textbox) like  SignIn,SignUp,Weather, Map ,Guide, API and City input etc
   weatherMap.validateObjects(arrMenus,weatherMap.strMenuLabelsCss,weatherMap.strLinkLbls)
      
  browser.sleep(100).then(callback);  
});

Given(/^Searching \"([^\"]*)\" City$/,function(strOption,callback)
{
    var city="";
    console.log(strOption)
    if(strOption ==="Invalid"){
         city="NGD";
    }
    else
    {
        city="Mumbai"
    }
    weatherMap.enterValue(weatherMap.searchCity,city);
    weatherMap.clickOnObject(weatherMap.searchBtn);
    browser.sleep(100).then(callback);
})

Then(/^Search result should \"([^\"]*)\" with any weather details$/,function(strResult,callback)
{
    if(strResult==="Not found")
    {
        elementHelper.waitForElement(element(by.css(".alert.alert-warning")));

        element(by.css(".alert.alert-warning")).getText().then(function(value)
        {
            var strNotFound=value.replace("/[^a-zA-Z ]/g", "").substring(1).trim()
            expect(strNotFound).to.equal(strResult);
        })
    }
    else{
        elementHelper.waitForElement(weatherMap.strTemperatureLbl);
        weatherMap.strTemperatureLbl.getText().then(function(text)
        {
            console.log(text)
            expect(text.includes("temperature")).to.be.true;
        })

        weatherMap.strTemperatureInCelsius.getText().then(function(text)
        {
            console.log(text)
            expect(text.includes("°С")).to.be.true;
        })
    }

    browser.sleep(100).then(callback);
})