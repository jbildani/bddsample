//import { browser } from 'protractor';

const EC = browser.ExpectedConditions;
/*
This class assists in waiting for non-angular page screen elements
 */
class ElementHelper {
	constructor(){


	this.waitForPresent=function(ele) {
		return browser.wait(EC.presenceOf(ele));
	}
	this.waitForDisplay=function(ele) {
		return browser.wait(ele.isDisplayed);
	}
	this.waitForElement=function(ele) {
		this.waitForPresent(ele);
		this.waitForDisplay(ele);
	}

	/*waitForPresent(ele) {
		return browser.wait(EC.presenceOf(ele));
	}
	waitForDisplay(ele) {
		return browser.wait(ele.isDisplayed);
	}
	waitForElement(ele) {
		this.waitForPresent(ele);
		this.waitForDisplay(ele);
	}*/
}

}

module.exports=new ElementHelper();
